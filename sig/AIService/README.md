# AI服务SIG

## SIG职责和目标
实现桌面操作系统上各类AI服务的基础软件栈，对下屏蔽各类硬件的异构性，对上为各类AI应用提供服务接口。

## SIG成员

### Owner
* longj@jideos.com

### Maintainers
* yangyc@jideos.com

### 邮件列表
aiservice@lists.openkylin.top
