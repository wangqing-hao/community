# RISC-V Multimedia（SIG）

RISC-V Multimedia 致力于riscv架构平台的开源多媒体框架（FFMPG/Gstreamer/OpenMax）集成及应用开发

## 工作目标

- 负责 openKylin 开源多媒体框架集成和适配。
- 负责 openKylin 软硬件编码器。

## 维护包列表


## SIG 成员

### Owner
- xiaoyong.zhu@starfivetech.com
### Maintainers
- skyler.zheng@starfivetech.com
- feng.zhou@starfivetech.com
- shuangzhi.xu@starfivetech.com
- selina.zheng@starfivetech.com

## 邮件列表

